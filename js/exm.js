
//variables
var Prom = 0.0;

var Altura = document.getElementById('altura');
var Peso = document.getElementById('peso');
var Edad = document.getElementById('edad');
var Imc = document.getElementById('imc');


function generar() {
  // Altura ___________________________
  const ARandom = [];

  ARandom.push(Math.random().toFixed(2) * (1.5 - 2.5) + 2.5);

  Altura = ARandom;
  document.getElementById('altura').value = Altura;

  // Edad ______________________________
  const ERandom = [];

  ERandom.push(Math.floor(Math.random() * (99 - 18) + 18));

  Edad = ERandom;
  document.getElementById('edad').value = Edad;

  // Peso ______________________________
  const PRandom = [];

  PRandom.push(Math.random().toFixed(2) * (19.9 - 129.9) + 129.9);

  Peso = PRandom;
  document.getElementById('peso').value = Peso;

}

function calcular() {


  var nivel = document.getElementById('nivel');

  document.getElementById('imc').value = (Peso / (Altura * Altura)).toFixed(2);

  Imc = document.getElementById('imc').value;

  if (Imc < 18.5) {

    document.getElementById('nivel').value = "Bajo peso";

  } else if (Imc >= 18.5 && Imc <= 24.9) {

    document.getElementById('nivel').value = "Peso saludable";

  } else if (Imc >= 25.0 && Imc <= 29.9) {

    document.getElementById('nivel').value = "Sobrepeso";

  } else if (Imc > 30.0) {

    document.getElementById('nivel').value = "Obesidad";

  }

}

var num = 0.0;

function registrar() {
  
  if (num <= 19) {

    num = num + 1;
    Prom = Number(Prom) + Number(Imc);
    document.getElementById('prom').value = (Prom / num).toFixed(2);

    const lista = document.querySelector("#lista1");
    const lista2 = document.createElement("lista2");

    lista2.textContent =  num + ". "+ "| Edad: " + Edad + " | Altura: " + Altura + " | Peso: " + Peso + " | IMC: " + document.getElementById('imc').value + " | Nivel: " + document.getElementById('nivel').value
    
    lista.appendChild(lista2);

  } else {

    alert("Ya ha alcanzado las 20 personas!");

  }
}

function Borrar() {
  const lista = document.querySelector("#lista1");
  lista.remove();
  alert("Se han borrado los registros!");
  location.reload();
}


